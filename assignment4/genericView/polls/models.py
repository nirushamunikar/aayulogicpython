from django.db import models
from django.contrib.auth.forms import UserCreationForm
class UploadImage(models.Model):
    name=models.CharField(max_length=200)
    address=models.CharField(max_length=200)
    title = models.CharField(max_length=50)
    image = models.ImageField(upload_to='path/', blank=False)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title



