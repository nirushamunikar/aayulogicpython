from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import UploadImage

class UploadImageForm(forms.ModelForm):
    class Meta:
        model=UploadImage
        fields=('name','address','title','image')

class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2' )

