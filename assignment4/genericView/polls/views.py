
from django.shortcuts import get_object_or_404, render,redirect
from django.contrib.auth import login, authenticate ,logout
from django.contrib.auth.views import login 
from django.http import HttpResponseRedirect,HttpResponse
from django.core.mail import send_mail,BadHeaderError
from django.core.mail import EmailMultiAlternatives
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView,DetailView
from .forms import UploadImageForm,SignUpForm
from .models import UploadImage

class ImageList(ListView):
    model=UploadImage
    template_name='polls/index.html'

def index(request):
    return render (request,'polls/index.html')

class About(DetailView):
    model=UploadImage
    template_name='polls/about.html'

def model_form_upload(request):
    if request.method == 'POST':
        form = UploadImageForm(request.POST,request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponse("success")
    else:
        form = UploadImageForm()

    return render(request, 'polls/form.html', {'form': form})

def email(request):
    subject, from_email, to = 'This is subject!!!', 'from@example.com', 'to@example.com'
    text_content = 'This is an important message.'
    html_content = '<p>This is an <strong>important</strong> message.</p>' # Formatting the text_content
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html") # How about formating your email with HTML?
    msg.attach('path/to/your/image', 'image/png') # To test, you can also send a workable image url
    # In this case, the second argument defines the type of attachement you want to send. We are sending an image file here
    msg.send()
    return HttpResponse("your message sent")

def signup(request):
    registered=False
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            user=form.save()
            user.set_password(user.password)  #password convert into hashvalue
            user.save()
            registered=True
            return HttpResponse("success")
        else:
            print("Invalid form request")
    else:
        form = SignUpForm()
    return render(request, 'polls/signup.html', {'form': form})

@login_required #login required garena vane chai /logout garyo vane tyo page ma janxa so first ma wu logged in vayo vane matra logout ma jana milxa
def user_logout(request):
    logout(request) #session is destroyed
    status=1




    
