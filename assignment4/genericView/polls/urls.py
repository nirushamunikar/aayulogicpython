from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'polls'
urlpatterns=[
   path('index/', views.index, name='index'),
#     path('<int:pk>/', views.DetailView.as_view(), name='detail'),
#     path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
#     path('<int:question_id>/vote/', views.vote, name='vote'),
    path('form/', views.model_form_upload, name='form'),
    path('email/', views.email, name='email'),
    path('signup/', views.signup, name='signup'),
    path('login/', views.login, {'template_name': 'polls/login.html'}, name='user_login'),
    path('about/<slug:pk>', views.About.as_view(), name='about'),
    path('list', views.ImageList.as_view(), name='imagelist'),

]